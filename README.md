DMP server that can be used as a base for modding or inherited to build modded docker images

## Whats new is
- Beta release

## Future features
- do a nice shutdown of the server when the docker stop command is issued.
- docker accessible commands to
   - start/stop/restart the server
   - send console commands to the server
   - look at console output from the server

## Starting the container

To run the latest stable version of this docker image run

	docker run --name dmp --restart=on-failure -p 6702:6702 \
              -v /path/to/universe:/app/dmp-server/Universe \
              -v /path/to/config:/app/dmp-server/Config \
              wearingstorm/ksp-dmp

the parameter

	-p 6702:6702

tell on witch external port the internal 6702 should be connected, in this case the same, if
you only type -p 6702 it will connect to a random port on the machine.

the parameter

    --restart=on-failure

will restart the docker container if failes

the parameter

    -v /path/to/universe:/app/dmp-server/Universe

will map the host universe folder to the dockers Universe folder and make the universe persistent

the parameter

    -v /path/to/config:/app/dmp-server/Config

will map the host config folder to the dockers Config folder to make it easy to use your config (optional)

## Giving the container a name

To make it easier to handle you container you can give it a name instead of the long
number thats normally give to it,

	--name dmp

to the run command to give it the name dmp, then you can start it easier with

	docker start dmp
	docker stop dmp


### Selecting version to download

If you want dont want the lastest docker image, but a specific docker version you can start the docker image by running,

	wearingstorm/ksp-dmp:{tag}
	
The default version is latest

## Source Repository
You are invited to contribute new features, fixes, or updates, large or small over at [GitLab](https://gitlab.com/wearingstorm/docker_ksp); we are always thrilled to receive pull requests, and do our best to process them as fast as we can.

## Known bugs