pipeline {
    agent {
        label 'docker'
    }
    options {
        timestamps()
        gitLabConnection('GitLab')
        buildDiscarder(logRotator(numToKeepStr:'25'))
    }
    triggers {
        cron('@weekly')
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')
    }
    environment {
        RELEASE_VERSION = 1.0
        GITLAB_REGISTRY = 'wearingstorm/docker_ksp'
        GITLAB_URL = 'https://registry.gitlab.com/wearingstorm/docker_ksp'
        DOCKER_URL = 'https://registry.hub.docker.com'
        DMP_DOWNLOAD_URL = 'http://godarklight.info.tm/dmp/downloads'
    }

    stages {
        stage('Preparing KSP-DMP') {
            steps {
                echo 'Downloading and installing KSP server'

                sh 'printenv'

                sh '''
                    curl -O "${DMP_DOWNLOAD_URL}/raycast-ground/DMPServer.zip"
                    curl -O "${DMP_DOWNLOAD_URL}/dmpupdater/DMPUpdater.exe"
                    mkdir dmp-server && 7za x DMPServer.zip && mv DMPServer/* dmp-server/ && mv DMPUpdater.exe dmp-server/ && rm DMPServer.zip && rm -rf DMPServer/
                    chmod u+x run.sh update.sh entry.sh
                   '''
            }
        }
        stage('Build image') {
            steps {
                script {
                    echo 'Building KSP-DMP docker image for branch ${BRANCH_NAME}'

                    if(BRANCH_NAME == 'master'){
                        docker.withRegistry(DOCKER_URL, 'docker-registry') {
                            def app = docker.build('wearingstorm/ksp-dmp')
                            app.push("weekly-${RELEASE_VERSION}.${BUILD_NUMBER}")
                            app.push("release-${RELEASE_VERSION}")
                            app.push("latest")
                        }
                    }
                    else if(BRANCH_NAME == 'develop'){
                        docker.withRegistry(DOCKER_URL, 'docker-registry') {
                            def app = docker.build('wearingstorm/ksp-dmp')
                            app.push("development")
                        }
                    }
                    else {
                        docker.withRegistry(GITLAB_URL, 'gitlab-registry') {
                            def app = docker.build('${GITLAB_REGISTRY}/${BRANCH_NAME}')
                            app.push("${BUILD_NUMBER}")
                        }
                    }
                }
            }
        }
    }
    post {
        always { 
            deleteDir() 
        } 
        success {
            slackSend channel: '#builds',
                      color: 'good',
                      message: "The pipeline ${currentBuild.fullDisplayName} completed successfully. (<${env.BUILD_URL}|Open>)"
        }
        failure {
            slackSend channel: '#builds',
                      color: '',
                      message: "The pipeline ${currentBuild.fullDisplayName} failed. (<${env.BUILD_URL}|Open>)"
        }
    }
}
