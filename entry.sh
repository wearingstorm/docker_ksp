#!/bin/bash
set -ex

# Change ownership and permissions and permission for mounted volumes/files
chown $USER:$USER -R /app/dmp-server/
chmod 755 /app/dmp-server/

# Updates DMP before start
sh /app/dmp-server/update.sh

# Execute the command passed as arguments
exec "$@"